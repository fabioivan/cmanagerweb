# coding=utf-8
from django.shortcuts import render_to_response, redirect
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.core.context_processors import csrf
from django.template import RequestContext


def login_user(request):
    state = "Por favor, faca login"
    username = password = ''
    c = {}
    c.update(csrf(request))
    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                state = "Voce foi logado com sucesso"
            else:
                state = "Sua conta não está ativa"
        else:
            state = "Seu usuario/senha esta incorreta"
    return render_to_response('auth.html', {'state': state, 'username': username},
                              context_instance=RequestContext(request))


def logout(request):
    logout(request)
    return redirect('/')


@login_required(login_url='/login/')
def home(request):
    if not request.user.is_authenticated():
        return redirect('/login/')
    else:
        return render_to_response('index.html', context_instance=RequestContext(request))
